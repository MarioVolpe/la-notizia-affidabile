package mariovolpe.la_notizia_affidabile;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import javax.net.ssl.HttpsURLConnection;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

/**
 * Created by rishabh on 31-01-2016.
 */
public class ReadRss extends AsyncTask<Void, Void, Void> {
    private Context context;
    private static final String REP = "https://www.repubblica.it/rss/homepage/rss2.0.xml";
    private static final String S24 = "https://www.ilsole24ore.com/rss/italia.xml";
    private String sorgente = S24;
    private ProgressDialog progressDialog;
    private ArrayList<FeedItem> feedItems;
    private RecyclerView recyclerView;

    ReadRss(Context context, RecyclerView recyclerView) {
        this.recyclerView = recyclerView;
        this.context = context;
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Loading...");
    }

    //before fetching of rss statrs show progress to user
    @Override
    protected void onPreExecute() {
        progressDialog.show();
        super.onPreExecute();
    }


    //This method will execute in background so in this method download rss feeds
    @Override
    protected Void doInBackground(Void... params) {
        //call process xml method to process document we downloaded from getData() method
        feedItems = new ArrayList<>();
        ProcessXml(Getdata());
        sorgente = REP;
        ProcessXml(Getdata());

        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        progressDialog.dismiss();
        FeedsAdapter adapter = new FeedsAdapter(context, feedItems);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.addItemDecoration(new VerticalSpace(20));
        recyclerView.setAdapter(adapter);

    }

    // In this method we will process Rss feed  document we downloaded to parse useful information from it
    private void ProcessXml(Document data) {
        if (data != null) {

            Element root = data.getDocumentElement();
            Node channel = root.getChildNodes().item(0);
            if(sorgente.equals("https://www.ilsole24ore.com/rss/italia.xml"))
                channel = root.getChildNodes().item(1);
            NodeList items = channel.getChildNodes();

            for (int i = 0; i < items.getLength(); i++) {
                Node cureentchild = items.item(i);

                if (cureentchild.getNodeName().equalsIgnoreCase("item")) {
                    FeedItem item=null;
                    if(sorgente.equals(S24)) {
                        item = s24Parser(cureentchild);
                    } else if (sorgente.equals(REP))
                        item = repParser(cureentchild);

                    feedItems.add(item);


                }
            }
        } else Log.e("data","null");
    }

    private FeedItem s24Parser(Node cureentchild) {
        NodeList itemchilds = cureentchild.getChildNodes();
        FeedItem item = new FeedItem();

        for (int j = 0; j < itemchilds.getLength(); j++) {
            Node cureent = itemchilds.item(j);
            Log.e("name",""+cureent.getNodeName());
            Log.e("content",""+cureent.getTextContent());

            if (cureent.getNodeName().equalsIgnoreCase("title")) {
                item.setTitle(cureent.getTextContent());
            } else if (cureent.getNodeName().equalsIgnoreCase("description")) {
                item.setDescription(cureent.getTextContent());
            } else if (cureent.getNodeName().equalsIgnoreCase("pubDate")) {
                item.setPubDate(cureent.getTextContent());
            } else if (cureent.getNodeName().equalsIgnoreCase("link")) {
                item.setLink(cureent.getTextContent());
            } else if (cureent.getNodeName().equalsIgnoreCase("media:thumbnail")) {
                //this will return us thumbnail url
                String url = cureent.getAttributes().item(0).getTextContent();
                item.setThumbnailUrl(url);
            }
        }
        String fonte = "ilsole24ore.com";
        item.setFonte(fonte);

        return item;
    }

    private FeedItem repParser(Node cureentchild) {
        NodeList itemchilds = cureentchild.getChildNodes();
        FeedItem item = new FeedItem();

        for (int j = 0; j < itemchilds.getLength(); j++) {
            Node cureent = itemchilds.item(j);

            if (cureent.getNodeName().equalsIgnoreCase("enclosure")){
                Node cureent2 = itemchilds.item(++j);
                //il prossimo è la descrizione mal formatatta, quindi la trattiamo
                String desc = cureent2.getTextContent();
                int index = desc.indexOf("<img");
                if(index!=-1) {
                    String url = desc.substring(index + 10, desc.indexOf("width")-2);
                    url = url.replaceAll("\"", "");
                    item.setThumbnailUrl(url);
                }

                index = desc.indexOf("align=\"left\"");
                if(index!=-1){
                    String descr = desc.substring(index+29, desc.indexOf("</p>"));
                    item.setDescription(descr);
                }

            } else if (cureent.getNodeName().equalsIgnoreCase("title")) {
                item.setTitle(cureent.getTextContent());
            } else if (cureent.getNodeName().equalsIgnoreCase("description")) {
                item.setDescription(cureent.getTextContent());
            } else if (cureent.getNodeName().equalsIgnoreCase("pubDate")) {
                item.setPubDate(cureent.getTextContent());
            } else if (cureent.getNodeName().equalsIgnoreCase("link")) {
                item.setLink(cureent.getTextContent());
            }
        }
        String fonte = "repubblica.it";
        item.setFonte(fonte);

        return item;
    }

    //This method will download rss feed document from specified url
    private Document Getdata() {
        try {
            URL url = new URL(sorgente);
            HttpsURLConnection sConnection;
            HttpURLConnection connection;
            InputStream inputStream;

            if(sorgente.contains("https")) {
                sConnection = (HttpsURLConnection) url.openConnection();
                sConnection.setRequestMethod("GET");
                inputStream = sConnection.getInputStream();
            }

            else {
                connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("GET");
                inputStream = connection.getInputStream();
            }

            DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = builderFactory.newDocumentBuilder();
            return builder.parse(inputStream);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("eccezione","IO");
            return null;
        }
    }
}
