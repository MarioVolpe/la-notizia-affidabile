package mariovolpe.la_notizia_affidabile;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by rishabh on 26-02-2016.
 */
public class FeedsAdapter extends RecyclerView.Adapter<FeedsAdapter.MyViewHolder> {
    private ArrayList<FeedItem>feedItems;
    private Context context;
    private static final String REP = "repubblica.it";
    private static final String S24 = "ilsole24ore.com";

    FeedsAdapter(Context context, ArrayList<FeedItem> feedItems){
        this.feedItems=feedItems;
        this.context=context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.custum_row_news_item,parent,false);
        return new MyViewHolder(view);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        YoYo.with(Techniques.FadeIn).playOn(holder.cardView);
        FeedItem current=feedItems.get(position);
        holder.Title.setText(current.getTitle());
        holder.Description.setText(current.getDescription());
        holder.Date.setText(current.getPubDate());
        holder.Fonte.setText(current.getFonte());
        if(current.getThumbnailUrl()==null) {
            if(current.getFonte().equals(REP))
                Picasso.with(context).load(R.drawable.larep).into(holder.Thumbnail);
            else if(current.getFonte().equals(S24))
                Picasso.with(context).load(R.drawable.s24).into(holder.Thumbnail);
        }
        else Picasso.with(context).load(current.getThumbnailUrl()).into(holder.Thumbnail);

    }



    @Override
    public int getItemCount() {
        return feedItems.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView Title,Description,Date,Fonte;
        ImageView Thumbnail;
        CardView cardView;
        MyViewHolder(View itemView) {
            //TODO: PULSANTE SHARE
            super(itemView);
            Title = itemView.findViewById(R.id.title_text);
            Description = itemView.findViewById(R.id.description_text);
            Date = itemView.findViewById(R.id.date_text);
            Thumbnail = itemView.findViewById(R.id.thumb_img);
            cardView = itemView.findViewById(R.id.cardview);
            Fonte = itemView.findViewById(R.id.fonte);


            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String url = feedItems.get(getAdapterPosition()).getLink();
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(url));
                    context.startActivity(i);
                }
            });
        }
    }
}
